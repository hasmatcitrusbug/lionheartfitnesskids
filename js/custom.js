/* Set the width of the side navigation to 250px */
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    //$('#nav-res').show().fadeIn('slow');
    document.getElementById("nav-res").style.opacity = "1";
}

/* Set the width of the side navigation to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
	document.getElementById("nav-res").style.opacity = "0";
	//$('#nav-res').hide().fadeIn('slow');
} 

/* Set the width of the side navigation to 250px */
function openlogout() {
  document.getElementById("mySidenav-logout").style.width = "100%";
  //$('#nav-res').show().fadeIn('slow');
  document.getElementById("nav-res").style.opacity = "1";
}

/* Set the width of the side navigation to 0 */
function closelogout() {
  document.getElementById("mySidenav-logout").style.width = "0";
  document.getElementById("nav-res").style.opacity = "0";
  document.getElementById("mySidenav").style.width = "0";
//$('#nav-res').hide().fadeIn('slow');
} 

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

$(document).ready(function(){
	
    $(".search-icon").click(function(){
        $(".search-filter").slideToggle(500);
    });
	
    $('.select2').select2();

    /* scroll */
    
    $("#video-img-1").on('hidden.bs.modal', function (e) {
      $("#video-img-1 iframe").attr("src", $("#video-img-1 iframe").attr("src"));
    }); 

});

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    document.getElementById("find-map").style.top = "210px";
  } else {
    document.getElementById("find-map").style.top = "255px";
  }
} 

$(window).scroll(function(){
  if ($(window).scrollTop() >= 300) {
    $('.header-top').addClass('fixed-header');
  }
  else {
    $('.header-top').removeClass('fixed-header');
  }
});



